"""Utility module."""
import constants


def check_pegs_are_ok(arr):
    """Check inconsistences in the code pattern or guess.

    :param arr:
        it should contain exactly `constants.SPACES` positions, each of which
        filled with a possible value in `constants.COLORS` constant
    :type arr: list

    :returns: True if `arr` is a valid input or False otherwise
    :rtype: bool

    """
    if len(arr) != constants.SPACES:
        return False

    for peg in arr:
        if peg not in constants.COLORS:
            return False

    return True
