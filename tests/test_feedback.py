"""Tests suite for feedback function."""

import constants


def test_no_allowed_http_method(client):
    """Test requesting with an invalid HTTP method."""
    r = client.get('/api/v1/codemaker/feedback/')
    assert r.status_code == 405


def test_no_game_created(client):
    """Make a guess without having a game created."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE', 'RED'],
              'uid': None}
    )
    assert r.status_code == 422
    assert r.json['message'] == \
        'No game has been created for that uid provided.'


def test_no_input(client):
    """Test when no input is provided."""
    r = client.post('/api/v1/codemaker/feedback/', data={})
    assert r.status_code == 422


def test_guess_not_match_number_of_pegs(client, create_game):
    """Test when the guess provided is not valid (number of pegs)."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 422


def test_guess_not_match_some_peg_color(client, create_game):
    """Test when the guess provided is not valid (invalid color of peg)."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE', 'ROJO'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 422


def test_guess_not_match_data_structure(client, create_game):
    """Test when the guess provided is not valid (data structure)."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': 'RED,GREEN,BLUE,RED',
              'uid': create_game['uid']}
    )
    assert r.status_code == 422


def test_check_feedback_after_two_guesses_without_win(client, create_game):
    """Two guesses without win the game. Check feedback them."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'BLUE', 'GREEN', 'BLUE'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 201
    assert r.json['finished'] is False
    assert r.json['whites'] == 2
    assert r.json['blacks'] == 1

    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE', 'BLUE'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 201
    assert r.json['finished'] is False
    assert r.json['whites'] == 0
    assert r.json['blacks'] == 3


def test_game_finish_with_a_win(client, create_game):
    """The game has been won and it is already finished."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE', 'RED'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 200
    assert r.json['finished'] is True


def test_guess_is_valid(client, create_game):
    """The guess provided is valid (number & color of pegs & structure)."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'BLUE', 'GREEN', 'BLUE'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 201

    assert r.json['blacks'] == 1
    assert r.json['whites'] == 2


def test_game_already_finished_with_a_win(client, create_game):
    """The game has been won and it is already finished."""
    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE', 'RED'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 200

    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE', 'RED'],
              'uid': create_game['uid']}
    )
    assert r.status_code == 422
    assert r.json['message'] == 'Game already finished. Start a new one.'


def test_max_attempts_reached(client, create_game):
    """We did the maximum attemps for guessing the code."""
    for attempt in range(constants.MAX_ATTEMPTS + 1):
        r = client.post(
            '/api/v1/codemaker/feedback/',
            data={'guess': ['RED', 'GREEN', 'BLUE', 'BLUE'],
                  'uid': create_game['uid']}
        )
    assert r.status_code == 422
