"""Entrypoint for running the Flask app."""
import os
import pickle
import uuid

import constants
from utils import check_pegs_are_ok

from flask import Flask, abort, jsonify, make_response, request, session

from flask_redis import FlaskRedis

app = Flask(__name__)
app.secret_key = '$up€r Secret Key'
app.config['REDIS_URL'] = 'redis://10.5.0.4:6379/0'
redis_store = FlaskRedis(app)


@app.route("/api/v1/codemaker/create/", methods=['POST'])
def create_game():
    """Codemaker function for create a new game.

    :http-methods:
        POST

    :param pattern:
        it must contain exactly `constants.SPACES` positions, each of which
        filled with a possible value in `constants.COLORS` constant
    :type pattern: list


    :returns:
        HTTP 201 and a descriptive message and the uid for that game
        if all is ok.

        HTTP 422 if there is some missing or malformed input
    :rtype:
        JSON with two keys: `message` which is 'Game created correctly.'
        and `uid` that identifies the game, if all went well.

        If something went wrong, then JSON with only `error` key

    """
    pattern = request.json and request.json['pattern'] or\
        request.form.getlist('pattern')
    if not check_pegs_are_ok(pattern):
        abort(422)

    # the pattern is valid, so create the game
    session.permanent = True
    session['uid'] = uuid.uuid4()

    # create a dict with the number of ocurrences for each color
    # this dict will be used in the algorithm of guessing
    pattern_count = dict()
    for peg in pattern:
        if peg not in pattern_count.keys():
            pattern_count[peg] = 0
        pattern_count[peg] = pattern_count[peg] + 1

    redis_store.set(
        session['uid'],
        pickle.dumps(
            {"pattern": pattern,
             "attempts": [],
             "finished": False,
             "pattern_count": pattern_count})
    )

    return make_response(jsonify(
        {'message': 'Game created correctly.',
         'uid': session['uid']}), 201)


@app.route("/api/v1/codemaker/feedback/", methods=['POST'])
def feedback():
    """Codemaker returns feedback to codebreaker.

    :http-methods:
        POST

    :param guess:
        it must contain exactly `constants.SPACES` positions, each of which
        filled with a possible value in `constants.COLORS` constant
    :type guess: list
    :param uid: uid assigned to the game (for unique identification)
    :type uid: string


    :returns:
        HTTP 200 if the guess match with the code and it has been register on
        redis
        HTTP 201 if the guess is correct and it has been register on redis
        HTTP 422 if there is some missing or malformed input
    :rtype:
        If the guess has been register on redis, then the following dict:

        `finished`: boolean for indicate if the game has just finished
        `blacks`: integer. number of blacks key pegs
        `whites`: integer. number of whites key pegs

        If something went wrong, then JSON with only `error` key

    """
    # check first if we have a game created for this session
    uid = request.json and request.json['uid'] or request.form.get('uid')
    data = redis_store.get(uid)
    if data is None:
        return make_response(
            jsonify(
                {'error': 'Unprocessable entity',
                 'message': 'No game has been created for that uid provided.'}
            ), 422)

    # the game has been created, so we can proced with the rest of the logic
    guess = request.json and request.json['guess'] or\
        request.form.getlist('guess')
    if not check_pegs_are_ok(guess):
        abort(422)

    data = pickle.loads(data)
    if data['finished']:
        return make_response(
            jsonify(
                {'error': 'Unprocessable entity',
                 'message': 'Game already finished. Start a new one.'}
            ), 422)

    # the input data provided is valid. we can proceed to report
    # feedback with the number of black & white pegs, following
    # MasterMind rules
    blacks = 0
    pattern_count = dict(data['pattern_count'])
    for i, peg in enumerate(guess):
        if data['pattern'][i] == peg:
            blacks += 1
            pattern_count[peg] -= 1
        elif data['pattern'][i] != peg and peg in pattern_count:
            if pattern_count[peg] > 0:
                pattern_count[peg] -= 1

    whites = constants.SPACES - blacks - sum(pattern_count.values())

    # calculate error code to return (422 or 200 in function if the game
    # has finished by a win or not) & if the game has been finished with
    # this guess (maybe we don't guess the code, but we reach the maximum
    # of tries)
    finished = len(data['attempts']) == constants.MAX_ATTEMPTS - 1 or\
        blacks == constants.SPACES
    error_code = (blacks == constants.SPACES and 200) or 201
    # save state on redis
    redis_store.set(
        uid,
        pickle.dumps({
            'pattern': data['pattern'],
            'attempts': data['attempts'] + [
                {'guess': guess,
                 'blacks': blacks,
                 'whites': whites}],
            'finished': finished,
            'pattern_count': data['pattern_count']}))

    return make_response(
        jsonify({
            'finished': finished,
            'blacks': blacks,
            'whites': whites}), error_code)


@app.route("/api/v1/board/historical/<string:uid>", methods=['GET'])
def historical(uid):
    """Retrieve historic for the current game.

    :http-methods:
        GET

    :param uid (optional):
        uid that identifies the game.
    :type uid: int


    :returns:
        HTTP 200 and a dict containing the list of `attemps` with black & white
        key pegs attached and a boolean `finished` for indicate if the game
        has been finished or not.

        HTTP 422 if you try to retrieve an historical game that has not been
        created yet.
    :rtype:
        JSON with two keys: `attempts` and `finished` if all went well.

        If something went wrong, the dict contains:
        `error` with the HTTP 422 error status message
        `message` which is 'No game has been created for now.'

    """
    data = redis_store.get(uid)
    if data is None:
        return make_response(
            jsonify(
                {'error': 'Unprocessable entity',
                 'message': 'No game has been created for now.'}
            ), 422)

    data = pickle.loads(data)
    return make_response(
        jsonify({
            'data': {
                'attempts': data['attempts'],
                'finished': data['finished']}
        }), 200)


@app.errorhandler(404)
def not_found(error):
    """Define HTTP 404 error in JSON mode."""
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(422)
def data_invalid(error):
    """Define HTTP 422 error in JSON mode."""
    return make_response(jsonify({'error': 'Unprocessable entity'}), 422)


if __name__ == "__main__":
    app.run(debug=os.environ.get('DEBUG', False), host='0.0.0.0')
