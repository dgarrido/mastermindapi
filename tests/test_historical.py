"""Tests suite for historical board function."""

import constants


def test_no_allowed_http_method(client):
    """Test requesting with an invalid HTTP method."""
    r = client.put('/api/v1/board/historical/abcd-1234a')
    assert r.status_code == 405


def test_no_game(client):
    """Test when no game is playing for now."""
    r = client.get('/api/v1/board/historical/abcd-1234a')
    assert r.status_code == 422
    assert r.json['message'] == 'No game has been created for now.'


def test_no_attempts_already(client, create_game):
    """Test when no attempts have been made for now."""
    r = client.get('/api/v1/board/historical/{}'.format(create_game['uid']))
    assert r.status_code == 200
    assert len(r.json['data']['attempts']) == 0
    assert r.json['data']['finished'] is False


def test_some_attempts_without_win(client, create_game):
    """Test doing some attempts to guess the code without success."""
    for i in range(3):
        r = client.post(
            '/api/v1/codemaker/feedback/',
            data={'guess': ['RED', 'GREEN', 'BLUE', 'BLUE'],
                  'uid': create_game['uid']}
        )

    r = client.get('/api/v1/board/historical/{}'.format(create_game['uid']))
    assert r.status_code == 200
    assert len(r.json['data']['attempts']) > 0
    assert r.json['data']['finished'] is False


def test_some_attempts_with_win(client, create_game):
    """Test doing some attempts to guess the code with success."""
    for i in range(3):
        r = client.post(
            '/api/v1/codemaker/feedback/',
            data={'guess': ['RED', 'GREEN', 'BLUE', 'BLUE'],
                  'uid': create_game['uid']}
        )

    r = client.post(
        '/api/v1/codemaker/feedback/',
        data={'guess': ['RED', 'GREEN', 'BLUE', 'RED'],
              'uid': create_game['uid']}
    )

    r = client.get('/api/v1/board/historical/{}'.format(create_game['uid']))
    assert r.status_code == 200
    assert len(r.json['data']['attempts']) > 0
    assert r.json['data']['finished'] is True


def test_expired_all_attempts_without_success(client, create_game):
    """Test doing all attempts to guess the code without success."""
    for i in range(constants.MAX_ATTEMPTS + 3):
        r = client.post(
            '/api/v1/codemaker/feedback/',
            data={'guess': ['RED', 'GREEN', 'BLUE', 'BLUE'],
                  'uid': create_game['uid']}
        )

    r = client.get('/api/v1/board/historical/{}'.format(create_game['uid']))
    assert r.status_code == 200
    assert len(r.json['data']['attempts']) == constants.MAX_ATTEMPTS
    assert r.json['data']['finished'] is True
