"""Definition of diferents constants for the MasterMind API."""

SPACES = 4
"""Define maximum number of holes."""

COLORS = ['RED', 'GREEN', 'BLUE', 'YELLOW', 'ORANGE', 'CYAN']
"""Define the peg colors which we can choose."""

MAX_ATTEMPTS = 12
"""Maximum number of attempts for resolve the hide code."""
