.. MasterMind API documentation master file, created by
   sphinx-quickstart on Fri Oct 19 11:12:53 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MasterMind API's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Main module / app.py
--------------------

.. automodule:: app
  :members:


Constants module / constants.py
-------------------------------

.. automodule:: constants
  :members:


Utils module / utils.py
-------------------------------

.. automodule:: utils
  :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
