"""Pytest configuration fixture."""
import json

from app import app

import pytest


@pytest.fixture
def client():
    """Define pytest fixture for use in each test."""
    app.config['TESTING'] = True
    client = app.test_client()

    yield client


@pytest.fixture
def create_game():
    """Create a new game in each test using this fixture."""
    r = app.test_client().post(
        '/api/v1/codemaker/create/',
        data={'pattern': ['RED', 'GREEN', 'BLUE', 'RED']}
    )

    yield json.loads(r.data)
