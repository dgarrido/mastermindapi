"""Tests suite for create game function."""


def test_no_allowed_http_method(client):
    """Test requesting with an invalid HTTP method."""
    r = client.get('/api/v1/codemaker/create/')
    assert r.status_code == 405


def test_no_input(client):
    """Test when no input is provided."""
    r = client.post('/api/v1/codemaker/create/', data={})
    assert r.status_code == 422


def test_pattern_not_match_number_of_pegs(client):
    """Test when the pattern provided is not valid (number of pegs)."""
    r = client.post(
        '/api/v1/codemaker/create/',
        data={'pattern': ['RED', 'GREEN', 'BLUE']}
    )
    assert r.status_code == 422


def test_pattern_not_match_some_peg_color(client):
    """Test when the pattern provided is not valid (invalid color of peg)."""
    r = client.post(
        '/api/v1/codemaker/create/',
        data={'pattern': ['RED', 'GREEN', 'BLUE', 'ROJO']}
    )
    assert r.status_code == 422


def test_pattern_not_match_data_structure(client):
    """Test when the pattern provided is not valid (data structure)."""
    r = client.post(
        '/api/v1/codemaker/create/',
        data={'pattern': 'RED,GREEN,BLUE,RED'}
    )
    assert r.status_code == 422


def test_pattern_is_valid(client):
    """The pattern provided is valid (number & color of pegs & structure)."""
    r = client.post(
        '/api/v1/codemaker/create/',
        data={'pattern': ['RED', 'GREEN', 'BLUE', 'RED']}
    )
    assert r.status_code == 201
