# MasterMind API

API that simulates the codemaker role of MasterMind game. Also, the API provide a endpoint to retrieve the historical of the game.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

As prerequisites, you will need to have installed in your system, Docker and Docker Compose.

### Installing

Simply run:

```
./bin/start
```

in order to launch the development enviroment.

For get a production enviroment, without debug, with nginx, etc., you must run then:

```
./bin/start-prod
```

## Running the tests

It is quite simple. Only launch:

```
./bin/run-tests
```

and it will run all the test suite.

## Examples of Usage

In this version, the codemaker has to choose a total of 4 colors. The colors can be: RED, GREEN, BLUE, YELLOW, ORANGE, CYAN.

**NOTE**: POST requests can be made with the parameters travelling in the form or as a JSON data.
**+ NOTE**: We are making requests directly to the flask app. If you run the production enviroment, you can also make request to `10.5.0.2` ip address on port `80`.

First, the codemaker must create a new game. Think about the pattern and then (in this one, we though making RED, RED, GREEN & BLUE as the pattern):

```
curl -d '{"pattern": ["RED", "RED", "GREEN", "BLUE"]}' -H "Content-Type: application/json" -X POST http://10.5.0.3:5000/api/v1/codemaker/create/
```

or also:

```
curl -d "pattern=RED&pattern=RED&pattern=GREEN&pattern=BLUE" -X POST http://10.5.0.3:5000/api/v1/codemaker/create/
```

Once the game has been created, it is quite important the retorned uid, because if a param mandatory for next API queries.

Next, we can try with a guess. For that (from now, we will be only making request with JSON data), and if your guessing is RED, BLUE, GREEN, RED, your requests should be:

**IMPORTANT NOTE**: You must replace the uid parameter with the returned from the game creation.

```
curl -d '{"guess": ["RED", "BLUE", "GREEN", "RED"], "uid": "abcd-12345"}' -H "Content-Type: application/json" -X POST http://10.5.0.3:5000/api/v1/codemaker/feedback/
```

You can do up to 12 (defined in constants) attempts trying to guess the code. Anytime, you can retrieve the historical:

**IMPORTANT NOTE**: You must replace the uid parameter with the appropiate (in this example is _abcd-12345_):

```
curl http://10.5.0.3:5000/api/v1/board/historical/abcd-12345
```

## Customizing

There are only three constants that you can customize. See [mastermindapi/constants.py](mastermindapi/constants.py) for more details

## Documentation

You can review all documentation at [docs/html/index.html](docs/html/index.html) .

## Built With

Flask & Redis.

## Authors

Only me, Daniel Garrido.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
